<?php
require_once 'libsurvey.php';

$page1 = new LibSurvey\Page('Page 1');
$page1->add(new LibSurvey\Blank('What is your name?'));
$page1->add(new LibSurvey\Choice('Are you male or female?', array('M' => 'Male', 'F' => 'Female')));

$page2 = new LibSurvey\Page('Page 2');
$page2->add(new LibSurvey\Blank('Why are you here?'));

$survey = new LibSurvey\Survey('Test Survey', array('save_to' => 'test.json'));
$survey->add($page1);
$survey->add($page2);

$survey->run();
